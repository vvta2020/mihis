package BibliotecaApp.control;

import BibliotecaApp.model.Carte;
import BibliotecaApp.repo.CartiRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.junit.jupiter.api.Assertions.*;

class BibliotecaCtrlTest {
    BibliotecaCtrl bc;

    @BeforeEach
    void setUp()
    {
        CartiRepo cr=new CartiRepo();
        bc=new BibliotecaCtrl(cr);
    }

    @Test
    void adaugaCarte_TC1_BB() {
        Carte c=new Carte();
        c.setTitlu("Ion");
        c.adaugaAutor("Liviu Rebreanu");
        c.setAnAparitie(Year.now().toString());
        c.setEditura("Humanitas");
        c.adaugaCuvantCheie("taran");

        try{
            int nrCarti=bc.getCarti().size();
            try{
                bc.adaugaCarte(c);
                assertEquals(nrCarti+1,bc.getCarti().size());
                System.out.println("carte adaugata cu succes");
            }catch (Exception e)
            {
                e.printStackTrace();
                assert(false);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Test
    void adaugaCarte_TC2_BB() {
        Carte c=new Carte();
        c.setTitlu("Ion");
        c.adaugaAutor("Liviu Rebreanu");
        c.setAnAparitie("1480");
        c.setEditura("Humanitas");
        c.adaugaCuvantCheie("taran");

        try{
            //int nrCarti=bc.getCarti().size();
            try{
                bc.adaugaCarte(c);
                assert(false);
            }catch (Exception e)
            {
                e.printStackTrace();
                assert(true);
                System.out.println("Exceptie");
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}