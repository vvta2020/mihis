package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CartiRepoMockTest {

    @Test
    void cautaCarte_TC1() {
        CartiRepoMock crm=new CartiRepoMock();
        List<Carte> carti_gasite=crm.cautaCarte("Calinescu");
        assertEquals(2,carti_gasite.size());
        assertEquals("Enigma Otiliei",carti_gasite.get(0).getTitlu());
        assertEquals("Test",carti_gasite.get(1).getTitlu());
    }

    @Test
    void cautaCarte_TC2() {
        CartiRepoMock crm=new CartiRepoMock(0);
        List<Carte> carti_gasite=crm.cautaCarte("Calinescu");
        assertEquals(0,carti_gasite.size());
    }
}