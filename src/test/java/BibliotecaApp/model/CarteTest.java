package BibliotecaApp.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;


import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CarteTest {
    Carte c1, c2, c3;

    @BeforeEach
    public void SetUp() {
        c1 = new Carte();
        c2 = null;
        c3 = new Carte();
        c3.setTitlu("Ion");
        System.out.println("Before Test");
    }

    @Test
    void getTitlu_simple1() {
        assertEquals("Ion", c3.getTitlu());
        System.out.println("c3 has title Ion");
    }

    @Test
    void getTitlu_simple2() {
        try {
            c2.getTitlu();
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("c2 does not have any title");
    }

    @Test
    void getTitlu_simple2v2() {
        assertThrows(NullPointerException.class,()->c2.getTitlu());
        System.out.println("c2 does not have any title");
    }

    @Test
    void getTitlu_simple3() {
        assertEquals("", c1.getTitlu());
        System.out.println("c1 has empty title");
    }

    @Disabled
    @Test
    void getTitlu_Disabled() {
        assertEquals(" ", c1.getTitlu());
        System.out.println("c1 has empty title");
    }


    @Test
    void getTitlu_compus() {
        assertEquals("Ion", c3.getTitlu());
        assertEquals("", c1.getTitlu());
        try {
            assertEquals("", c2.getTitlu());
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("3 gettitle tests ok");
    }


    @Test
    void getTitlu_testeaza_toate() {
        assertAll("Carte",
                ()->assertEquals("",c1.getTitlu()),
                ()->assertEquals(null, c2),
                ()->assertEquals("Ion",c3.getTitlu()));
        System.out.println("Test all title ok");
    }

    @Test
    void setTitlu() {
        assertEquals("", c1.getTitlu());
        c1.setTitlu("Poezii");
        assertNotEquals("", c1.getTitlu());
        assertEquals("Poezii", c1.getTitlu());
        System.out.println("settitle ok");
    }

    @Test
    void constrCarte() {
        Carte aux = new Carte();
        assertEquals(c1.getTitlu(), aux.getTitlu());
        assertNotEquals(null, aux);
        System.out.println("constr. test ok");
    }

    @AfterEach
    public void TearDown() {
        c1 = null;
        c2 = null;
        c3 = null;
        System.out.println("After Test");
    }


    //timeout: varianta cu adnotarea Timeout
    @Test
    @Order (2)
    //@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    //@Timeout(5)
    public void cautaDupaAutor() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //nu avem nici un autor pentru cartea c1
        assertEquals(false,c1.cautaDupaAutor("Ion Creanga"));
        System.out.println("search with timeout ok");
    }

    //timeout: varianta cu metoda asserTimeout
    @Test
    public void cautaDupaAutor_cu_assertTimeout() {
        Assertions.assertTimeout(Duration.ofSeconds(2), () -> {
            delay_and_call();
        }, String.valueOf(false)); //Nu avem nici un autor pt. c1
        System.out.println("search with assertTimeout ok");
    }

    private void delay_and_call() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ;
        assertEquals(false,c1.cautaDupaAutor("Ion Creanga"));
    }

    //vezi si https://www.baeldung.com/parameterized-tests-junit-5

    @ParameterizedTest
    @Order (1)
    @ValueSource(strings = {"Ion", "Amintiri din copilarie", "Poezii"})
    void testParametrizatSetandGetTitlu(String titlu) {
        c1.setTitlu(titlu);
        assertEquals(titlu, c1.getTitlu());
        System.out.println("Parametrized Set&Get with "+titlu+" ok");
    }


}